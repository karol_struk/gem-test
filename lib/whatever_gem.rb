require "whatever_gem/version"

module WhateverGem

  def self.hello_world
    puts "Hello World from whatever gem!"
  end

  module MagicUrl
    def get_url(path)
      "#{Rails.configuration.es_scheme}://#{Rails.configuration.es_domain}#{event_service_paths[path]}"
    end

    def get_pixel_img(params, event = :open_email)
      url = ''
      case event
        when :open_email
          url = get_open_email_pixel_url params
        else
          #     nothing to do
      end
      "<img src=\"#{url}\" width=1px height=1px style=\"position:absolute; visibility:hidden\"/>"
    end

    def get_template_click_in_email_proxy_url(params)
      "#{get_url :proxies}?#{{data: click_in_email_event_params(params)}.to_query}"
    end

    def get_click_in_email_proxy_url(params)
      "#{get_url :proxies}?#{{data: parse_params(click_in_email_event_params(params))}.to_query}"
    end

    def get_open_email_pixel_url(params)
      "#{get_url :pixels}?#{{data: parse_params(open_email_event_params(params))}.to_query}"
    end

    def get_custom_link_url(params)
      "#{get_url :proxies}?#{{data: parse_params(link_custom_event_params(params))}.to_query}"
    end

    private
    def event_service_paths
      {
          pixels: '/api/v1/pixels',
          proxies: '/api/v1/proxies'
      }
    end

    def open_email_event_params(params, sdk = :pixel)
      {
          type: :email,
          action: :opened,
          sdk: sdk,
          platform: :frontend,
          subtype: params[:subtype],
          recipient: params[:recipient],
          id_uniq: params[:id_uniq],
          subject: params[:subject],
          vid: params[:vid] || get_email_vid(self.account, params[:recipient]),
          account: self.account.to_s,
          sender: params[:sender],
          campaign: params[:campaign],
          template: params[:template],
          list_config: params[:list_config],
          email_config: params[:email_config],
          hidden: params[:hidden]
      }
    end

    def click_in_email_event_params(params, sdk = :proxy)
      {
          type: :email,
          action: :link_clicked,
          sdk: sdk,
          platform: :frontend,
          subtype: params[:subtype],
          recipient: params[:recipient],
          id_uniq: params[:id_uniq],
          subject: params[:subject],
          vid: params[:vid] || get_email_vid(self.account, params[:recipient]),
          account: self.account.to_s,
          sender: params[:sender],
          email_config: params[:email_config],
          template: params[:template],
          link: params[:link],
          scenarios: params[:scenarios] || [],
          campaign: params[:campaign],
          pnt_redirect_params: {
              pnt_vid: params[:person_vid],
              utm_source: "personate_#{params[:subtype].to_s}",
              utm_medium: :email,
              utm_campaign: params[:campaign] || :none
          },
          list_config: params[:list_config],
          hidden: params[:hidden]
      }
    end

    def link_custom_event_params(params, sdk = :proxy)
      {
          type: :custom,
          platform: :frontend,
          sdk: sdk,
          account: self.account.to_s,
          subtype: params[:subtype],
          action: params[:action],
          label: params[:label],
          vid: params[:vid],
          meta: params[:meta] || {},
          link: params[:link],
          campaign: params[:campaign],
          pnt_redirect_params: {
              pnt_vid: params[:person_vid],
              utm_source: "personate_#{params[:source]}",
              utm_medium: params[:label] || :link,
              utm_campaign: params[:campaign] || :none
          },
          hidden: params[:hidden]
      }
    end

    def parse_params(params)
      encoded_params = {}
      params.each do |key, val|
        encoded_params[key.to_s.camelize(:lower).to_sym] = get_encoded_value val
      end
      encoded_params
    end

    def get_encoded_value(value)
      unless value.nil?
        if value.is_a? Hash
          parse_hash value
        elsif value.is_a? Array
          parse_array value
        else
          Base64.encode64 value.to_s
        end
      end
    end

    def parse_array(value)
      value.map { |val|
        unless val.nil?
          if val.is_a? Hash
            parse_hash val
          elsif val.is_a? Array
            parse_array val
          else
            Base64.encode64 val.to_s
          end
        end
      }
    end

    def parse_hash(value)
      data = {}
      value.each do |key, val|
        unless val.nil?
          if val.is_a? Hash
            data[key.to_s.camelize(:lower).to_sym] = parse_hash val
          elsif val.is_a? Array
            data[key.to_s.camelize(:lower).to_sym] = parse_array val
          else
            data[key.to_s.camelize(:lower).to_sym] = Base64.encode64 val.to_s
          end
        end
      end
      data
    end
  end

  class Client
    include WhateverGem::MagicUrl
    attr_accessor :account

    def initialize(account)
      @account = account
      #   todo raise Exception when account is invalid
    end

    def handmade(vid, subtype, comment, happened_at)
      data = {
          type: :handmade,
          vid: vid,
          subtype: subtype,
          comment: comment,
          happenedAt: happened_at
      }.merge(base_params)
      api_call({data: data})
    end


    # Run *email* to add event _type_ _email_ in *ES*
    # attribute *args* need to have:
    # +recipient+:: recipient e-mail
    # +sender+:: sender e-mail
    # if *subtype* is _campaign_ *args* need to have:
    # +campaign+:: *campaign* id as a _String_
    # if *action* is _link_click_ *args* need to have:
    # +link+:: clicked *link* in email
    # attribute *args* can have also:
    # +subject+:: email subject
    # +template+:: *template_id* from _email_ _service_
    # +html+:: email html
    # +parameters+:: person parameters sent to _email_ _service_
    # +response+:: _email_ _service_ answer
    # +scenarios+:: send it if it's a part of scenario
    def email(subtype, action, id_uniq, args = {})
      data = {
          type: :email,
          subtype: subtype,
          action: action,
          vid: get_email_vid(self.account, args[:recipient]),
          idUniq: id_uniq,
          recipient: args[:recipient],
          subject: args[:subject],
          sender: args[:sender],
          campaign: args[:campaign],
          template: args[:template],
          link: args[:link],
          scenarios: args[:scenarios] || [],
          html: args[:html],
          parameters: args[:parameters],
          response: args[:response],
          listConfig: args[:list_config],
          emailConfig: args[:email_config],
          hidden: args[:hidden]
      }.merge(base_params)
      api_call({data: data})
    end

    def recipe_finished(vid, fid, scenario_id)
      data = {
          type: :recipe_finished,
          vid: vid,
          fid: fid,
          scenarioId: scenario_id,
          hidden: true
      }.merge(base_params)
      api_call({data: data})
    end

    def goal(vid, gid, action, label, args={})
      data = {
          type: :goal,
          vid: vid,
          gid: gid,
          action: action,
          label: label,
          scenarios: args[:scenarios] || []
      }.merge(base_params)
      api_call({data: data})
    end

    def tag(vid, subtype, action, tag, args={})
      data = {
          type: :tag,
          subtype: subtype, #auto/manual
          action: action,
          vid: vid,
          name: tag,
          scenarios: args[:scenarios] || []
      }.merge(base_params)
      api_call({data: data})
    end

    def custom(vid, subtype, action, label, args={})
      data = {
          type: :custom,
          platform: args[:platform] || :backend,
          sdk: :ruby,
          account: self.account.to_s,
          subtype: subtype,
          action: action,
          label: label,
          vid: vid,
          scenarios: args[:scenarios] || [],
          meta: args[:meta] || {}
      }

      data.merge! ({
          happenedAt: args[:happened_at]
      }) if args[:happened_at].present?

      api_call({data: data})
    end

    private
    def api_call(data)
      json_data = data ? data.to_json : ''
      WhateverGem::ApiWorker.perform_async json_data
    end

    def get_email_vid(account, email)
      Base64.encode64(account.to_s+email.downcase).strip
    end

    def base_params
      {
          platform: :backend,
          sdk: :ruby,
          account: self.account.to_s
      }
    end
  end

  class ApiWorker
    include Sidekiq::Worker

    URL = "#{Rails.configuration.es_scheme}://#{Rails.configuration.es_domain}"
    ENDPOINT = Rails.configuration.es_reporter_path

    def perform(params)
      conn = Faraday.new(url: URL) do |faraday|
        faraday.request :url_encoded
        faraday.adapter Faraday.default_adapter
      end

      res = conn.send(:post) do |req|
        req.url ENDPOINT
        req.headers['Content-Type'] = 'application/json'
        req.body = params
      end

      JSON.parse res.body
    rescue Faraday::ClientError => e
      # todo: do something smart when connection has failed
      {
          message: e.message
      }
    end
  end
end
